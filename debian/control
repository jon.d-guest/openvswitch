Source: openvswitch
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Ben Pfaff <pfaffben@debian.org>,
 Simon Horman <horms@debian.org>,
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 autoconf,
 automake,
 bzip2,
 debhelper (>= 10),
 dh-python,
 graphviz,
 libssl-dev,
 libtool,
 openssl,
 openstack-pkg-tools,
 procps,
 python-all,
 python-all-dev,
 python-setuptools,
 python-six,
 python-twisted-conch,
 python-zope.interface,
 python3-all,
 python3-all-dev,
 python3-setuptools,
 python3-six,
 python3-sphinx,
 python3-zope.interface,
Standards-Version: 4.1.1
Vcs-Browser: https://salsa.debian.org/openstack-team/third-party/openvswitch
Vcs-Git: https://salsa.debian.org/openstack-team/third-party/openvswitch.git
Homepage: http://openvswitch.org/

Package: openvswitch-common
Architecture: linux-any
Depends:
 openssl,
 python-six,
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Suggests:
 ethtool,
Replaces:
 openvswitch-switch (<< 2.10.0+2018.08.28+git.8ca7c82b7d+ds1),
 openvswitch-test (<< 2.8.0),
 openvswitch-testcontroller (<< 2.8.0),
 openvswitch-vtep (<< 2.10.0+2018.08.28+git.8ca7c82b7d+ds1),
 ovn-central (<< 2.8.0),
 ovn-common (<< 2.8.0),
 ovn-controller-vtep (<< 2.8.0),
 ovn-docker (<< 2.8.0),
 ovn-host (<< 2.8.0),
Breaks:
 openvswitch-switch (<< 2.10.0+2018.08.28+git.8ca7c82b7d+ds1),
 openvswitch-test (<< 2.8.0),
 openvswitch-testcontroller (<< 2.8.0),
 openvswitch-vtep (<< 2.10.0+2018.08.28+git.8ca7c82b7d+ds1),
 ovn-central (<< 2.8.0),
 ovn-common (<< 2.8.0),
 ovn-controller-vtep (<< 2.8.0),
 ovn-docker (<< 2.8.0),
 ovn-host (<< 2.8.0),
Provides:
 openvswitch-test,
 ovn-common,
 ovn-docker,
Description: Open vSwitch common components
 Open vSwitch is a production quality, multilayer, software-based,
 Ethernet virtual switch. It is designed to enable massive network
 automation through programmatic extension, while still supporting
 standard management interfaces and protocols (e.g. NetFlow, IPFIX,
 sFlow, SPAN, RSPAN, CLI, LACP, 802.1ag). In addition, it is designed
 to support distribution across multiple physical servers similar to
 VMware's vNetwork distributed vswitch or Cisco's Nexus 1000V.
 .
 openvswitch-common provides components required by both openvswitch-switch
 and openvswitch-testcontroller.

Package: openvswitch-dbg
Section: debug
Architecture: linux-any
Depends:
 openvswitch-common (= ${binary:Version}),
 openvswitch-switch (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 openvswitch-testcontroller (<< ${binary:Version}),
 openvswitch-testcontroller (>> ${binary:Version}),
Description: Debug symbols for Open vSwitch packages
 Open vSwitch is a production quality, multilayer, software-based,
 Ethernet virtual switch. It is designed to enable massive network
 automation through programmatic extension, while still supporting
 standard management interfaces and protocols (e.g. NetFlow, IPFIX,
 sFlow, SPAN, RSPAN, CLI, LACP, 802.1ag). In addition, it is designed
 to support distribution across multiple physical servers similar to
 VMware's vNetwork distributed vswitch or Cisco's Nexus 1000V.
 .
 This package contains the debug symbols for all the other openvswitch-*
 packages.  Install it to debug one of them or to examine a core dump
 produced by one of them.

Package: openvswitch-dev
Architecture: linux-any
Depends:
 openvswitch-common (>= ${binary:Version}),
 ${misc:Depends},
Description: Open vSwitch development package
 Open vSwitch is a production quality, multilayer, software-based, Ethernet
 virtual switch. It is designed to enable massive network automation through
 programmatic extension, while still supporting standard management interfaces
 and protocols (e.g. NetFlow, sFlow, SPAN, RSPAN, CLI, LACP, 802.1ag). In
 addition, it is designed to support distribution across multiple physical
 servers similar to VMware's vNetwork distributed vswitch or Cisco's Nexus
 1000V.
 .
 This package provides openvswitch headers and libopenvswitch for developers.

Package: openvswitch-pki
Architecture: all
Depends:
 openvswitch-common (<< ${source:Version}.1~),
 openvswitch-common (>= ${source:Version}),
 ${misc:Depends},
Description: Open vSwitch public key infrastructure dependency package
 Open vSwitch is a production quality, multilayer, software-based,
 Ethernet virtual switch. It is designed to enable massive network
 automation through programmatic extension, while still supporting
 standard management interfaces and protocols (e.g. NetFlow, IPFIX,
 sFlow, SPAN, RSPAN, CLI, LACP, 802.1ag). In addition, it is designed
 to support distribution across multiple physical servers similar to
 VMware's vNetwork distributed vswitch or Cisco's Nexus 1000V.
 .
 openvswitch-pki provides PKI (public key infrastructure) support for
 Open vSwitch switches and controllers, reducing the risk of
 man-in-the-middle attacks on the Open vSwitch network infrastructure.

Package: openvswitch-switch
Architecture: linux-any
Depends:
 kmod,
 lsb-base,
 netbase,
 openvswitch-common (= ${binary:Version}),
 procps,
 python:any,
 uuid-runtime,
 ${misc:Depends},
 ${python3:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Open vSwitch switch implementations
 Open vSwitch is a production quality, multilayer, software-based,
 Ethernet virtual switch. It is designed to enable massive network
 automation through programmatic extension, while still supporting
 standard management interfaces and protocols (e.g. NetFlow, IPFIX,
 sFlow, SPAN, RSPAN, CLI, LACP, 802.1ag). In addition, it is designed
 to support distribution across multiple physical servers similar to
 VMware's vNetwork distributed vswitch or Cisco's Nexus 1000V.
 .
 openvswitch-switch provides the userspace components and utilities for
 the Open vSwitch kernel-based switch.

Package: openvswitch-testcontroller
Architecture: linux-any
Depends:
 lsb-base,
 openvswitch-common (= ${binary:Version}),
 openvswitch-pki (<< ${source:Version}.1~),
 openvswitch-pki (>= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Simple controller for testing OpenFlow setups
 This controller enables OpenFlow switches that connect to it to act
 as MAC-learning Ethernet switches.  It can be used for initial
 testing of OpenFlow networks.  It is not a necessary or desirable
 part of a production OpenFlow deployment.

Package: openvswitch-vtep
Architecture: linux-any
Depends:
 lsb-base,
 openvswitch-common (= ${binary:Version}),
 openvswitch-switch (= ${binary:Version}),
 python-openvswitch (<< ${source:Version}.1~),
 python-openvswitch (>= ${source:Version}),
 python:any,
 ${misc:Depends},
 ${python3:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Open vSwitch VTEP utilities
 Open vSwitch is a production quality, multilayer, software-based, Ethernet
 virtual switch. It is designed to enable massive network automation through
 programmatic extension, while still supporting standard management interfaces
 and protocols (e.g. NetFlow, sFlow, SPAN, RSPAN, CLI, LACP, 802.1ag). In
 addition, it is designed to support distribution across multiple physical
 servers similar to VMware's vNetwork distributed vswitch or Cisco's Nexus
 1000V.
 .
 This package provides utilities that are useful to interact with a
 VTEP-configured database and a VTEP emulator.

Package: ovn-central
Architecture: linux-any
Depends:
 lsb-base,
 openvswitch-common (= ${binary:Version}),
 openvswitch-switch (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: OVN central components
 OVN, the Open Virtual Network, is a system to support virtual network
 abstraction.  OVN complements the existing capabilities of OVS to add
 native support for virtual network abstractions, such as virtual L2 and L3
 overlays and security groups.
 .
 ovn-central provides the userspace daemons, utilities and
 databases for OVN that is run at a central location.

Package: ovn-controller-vtep
Architecture: linux-any
Depends:
 lsb-base,
 openvswitch-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: OVN vtep controller
 ovn-controller-vtep is the local controller daemon in
 OVN, the Open Virtual Network, for VTEP enabled physical switches.
 It connects up to the OVN Southbound database over the OVSDB protocol,
 and down to the VTEP database over the OVSDB protocol.
 .
 ovn-controller-vtep provides the ovn-controller-vtep binary for controlling
 vtep gateways.

Package: ovn-host
Architecture: linux-any
Depends:
 lsb-base,
 openvswitch-common (= ${binary:Version}),
 openvswitch-switch (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: OVN host components
 OVN, the Open Virtual Network, is a system to support virtual network
 abstraction.  OVN complements the existing capabilities of OVS to add
 native support for virtual network abstractions, such as virtual L2 and L3
 overlays and security groups.
 .
 ovn-host provides the userspace components and utilities for
 OVN that can be run on every host/hypervisor.

Package: python-openvswitch
Architecture: all
Section: python
Depends:
 python-six,
 ${misc:Depends},
 ${python:Depends},
Replaces:
 openvswitch-test (<< 2.8.0),
Breaks:
 openvswitch-test (<< 2.8.0),
Description: Python bindings for Open vSwitch
 Open vSwitch is a production quality, multilayer, software-based,
 Ethernet virtual switch. It is designed to enable massive network
 automation through programmatic extension, while still supporting
 standard management interfaces and protocols (e.g. NetFlow, IPFIX,
 sFlow, SPAN, RSPAN, CLI, LACP, 802.1ag). In addition, it is designed
 to support distribution across multiple physical servers similar to
 VMware's vNetwork distributed vswitch or Cisco's Nexus 1000V.
 .
 This package contains the full Python bindings for Open vSwitch database.

Package: python3-openvswitch
Architecture: all
Section: python
Depends:
 python3-six,
 ${misc:Depends},
 ${python3:Depends},
Description: Python 3 bindings for Open vSwitch
 Open vSwitch is a production quality, multilayer, software-based,
 Ethernet virtual switch. It is designed to enable massive network
 automation through programmatic extension, while still supporting
 standard management interfaces and protocols (e.g. NetFlow, IPFIX,
 sFlow, SPAN, RSPAN, CLI, LACP, 802.1ag). In addition, it is designed
 to support distribution across multiple physical servers similar to
 VMware's vNetwork distributed vswitch or Cisco's Nexus 1000V.
 .
 This package contains the full Python 3 bindings for Open vSwitch database.
