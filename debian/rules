#!/usr/bin/make -f

UPSTREAM_GIT = https://github.com/openvswitch/ovs
include /usr/share/openstack-pkg-tools/pkgos.make

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
PARALLEL = -j$(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
else
PARALLEL =
endif

export DEB_BUILD_MAINT_OPTIONS=hardening=+bindnow

%:
	dh $@ --with autoreconf,python2,python3,systemd,sphinxdoc --parallel

# use --as-needed only if supported by dh-autoreconf (to simplify backporting)
DH_AS_NEEDED=$(shell dpkg --compare-versions $$(dpkg --status dh-autoreconf | grep Version | cut -d' ' -f2) ge 6 && echo --as-needed)
override_dh_autoreconf:
	dh_autoreconf $(DH_AS_NEEDED)

override_dh_auto_configure:
	dh_auto_configure -- --enable-ssl --enable-shared $(DATAPATH_CONFIGURE_OPTS)

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	if $(MAKE) check TESTSUITEFLAGS='$(PARALLEL)' RECHECK=yes; then :; \
	else \
		cat tests/testsuite.log; \
		exit 1; \
	fi
endif

override_dh_auto_install:
	set -e && cd python && python2.7 setup.py install --install-layout=deb --root $(CURDIR)/debian/python-openvswitch; cd ..
	set -e && for pyvers in $(PYTHON3S); do \
		cd python && python$$pyvers setup.py install --install-layout=deb \
			--root $(CURDIR)/debian/python3-openvswitch; cd ..; \
	done
	rm -rf $(pwd)/debian/python*/usr/lib/python*/dist-packages/*.pth

override_dh_installinit:
	dh_installinit -R
	dh_installinit -popenvswitch-switch --name=openvswitch-nonetwork --no-start

override_dh_auto_build:
	$(MAKE)

override_dh_auto_clean:
	dh_auto_clean
	set -e ; make python/ovs/version.py && cd python && python setup.py clean ; rm -f ovs/version.py ; cd ..

override_dh_install:
	DESTDIR=$(CURDIR)/debian/tmp $(MAKE) install
	install -D -m 0644 utilities/ovs-vsctl-bashcomp.bash $(CURDIR)/debian/openvswitch-switch/usr/share/bash-completion/completions/ovs-vsctl
	dh_install
	rm -f $(CURDIR)/debian/tmp/usr/lib/*/*.la
	dh_installman --language=C
	rm -rf $(CURDIR)/debian/tmp/usr/share/man
	dh_missing --fail-missing
	# openvswitch-switch
	mkdir -p debian/openvswitch-switch/usr/share/openvswitch/switch
	cp debian/openvswitch-switch.template debian/openvswitch-switch/usr/share/openvswitch/switch/default.template

	# ovn-host
	mkdir -p debian/ovn-host/usr/share/ovn/host
	cp debian/ovn-host.template debian/ovn-host/usr/share/ovn/host/default.template

	# ovn-central
	mkdir -p debian/ovn-central/usr/share/ovn/central
	cp debian/ovn-central.template debian/ovn-central/usr/share/ovn/central/default.template

override_dh_installinit:
	dh_installinit -R
	dh_installinit -popenvswitch-switch --name=openvswitch-nonetwork --no-start

override_dh_strip:
	dh_strip --dbg-package=openvswitch-dbg

override_dh_installman:
	echo "Do nothing..."

override_dh_python2:
	dh_python2
	dh_python2 -popenvswitch-ipsec  /usr
	dh_python2 -popenvswitch-vtep   /usr
	dh_python2 -popenvswitch-switch /usr

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
	dh_python3 usr/share/openvswitch/scripts --shebang=/usr/bin/python3
